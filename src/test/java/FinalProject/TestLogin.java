package FinalProject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;


public class TestLogin {
    WebDriver driver;

    @BeforeClass
    public void openBrowser(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
    }

    @AfterMethod
    public void logout() throws InterruptedException{
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(1000);
    }

    @Test(priority = 0)
    public void TC_01(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
        driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();
        String username = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(),"Make Appointment");
    }

    @Test(priority = 1)
    public void TC_02(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
        driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();
        String password = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("username salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(),"Login failed! Please ensure the username and password are valid.");
    }

    @Test(priority = 2)
    public void TC_03(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
        driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();
        String username = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("password");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(),"Login failed! Please ensure the username and password are valid.");
    }

    @Test(priority = 3)
    public void TC_04(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
        driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("username");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("password");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(),"Login failed! Please ensure the username and password are valid.");
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException{
        Thread.sleep(3000);
        driver.quit();
    }
}
