package FinalProject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;

public class HistoryPage {
    WebDriver driver;

    @BeforeClass
    public void openBrowser(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
    }

    @AfterMethod
    public void logout() throws InterruptedException{
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(5000);
    }

    @Test(priority = 0)
    public void TC_06(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/");
        driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();
        String username = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();
        Select facility = new Select(driver.findElement(By.xpath("//*[@id=\"combo_facility\"]")));
        facility.selectByIndex(1);
        driver.findElement(By.xpath("//*[@id=\"chk_hospotal_readmission\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"radio_program_medicaid\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/form/div[4]/div/div/div/span")).click();
        driver.findElement(By.xpath("/html/body/div/div[1]/table/tbody/tr[5]/td[6]")).click();
        driver.findElement(By.xpath("//*[@id=\"txt_comment\"]")).sendKeys("Test Appointment");

        driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"summary\"]/div/div/div[1]/h2")).getText(),"Appointment Confirmation");

        driver.findElement(By.xpath("//*[@id=\"summary\"]/div/div/div[7]/p/a")).click();
        driver.findElement(By.xpath("//*[@id=\"menu-toggle\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"sidebar-wrapper\"]/ul/li[3]/a")).click();


        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"facility\"]")).getText(),"Hongkong CURA Healthcare Center");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"hospital_readmission\"]")).getText(),"Yes");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"program\"]")).getText(),"Medicaid");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"history\"]/div/div[2]/div[1]/div/div[1]")).getText(),"30/12/2022");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"comment\"]")).getText(),"Test Appointment");
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException{
        Thread.sleep(3000);
        driver.quit();
    }
}
